/**
 * Created with JetBrains PhpStorm.
 * User: Aaron
 * Date: 8/14/13
 * Time: 11:01 AM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function(){

    $('.add').on('click', function(){



        $.ajax({
            url: '/admin/phonebook/ajax_add',
            type: 'POST',
            dataType: 'json',
            data:
            {
                name:      $(".name").val(),
                phone:     $(".phone").val(),
                email:     $(".email").val(),
                address:   $(".address").val(),
                state:     $(".state").val(),
                zip:       $(".zip").val()
            },
            success: function(output)
            {
             var string= '';
                string+='<div class="alert success animated fadeIn">' + output.message + '</div>';
                $('#error').html('').prepend(string);

            },
            error: function (output)
            {
                var string = '';
                string+='<div class="alert error animated fadeIn">' + output.responseText + '</div>';
                $('#error').html('').prepend(string);
            }

        });
        var tr = $(this).closest('tr');
        tr.find(":text").attr('value', null);
    });



 });