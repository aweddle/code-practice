/**
 * Created with JetBrains PhpStorm.
 * User: Aaron
 * Date: 8/12/13
 * Time: 2:07 PM
 * To change this template use File | Settings | File Templates.
 */
// updates page, no refresh on update page



$(document).ready(function(){

    $(".submit").hide();

    $('.name').attr('disabled', true);
    $('.phone').attr('disabled', true);
    $('.email').attr('disabled', true);
    $('.address').attr('disabled', true);
    $('.state').attr('disabled', true);
    $('.zip').attr('disabled', true);

//    $("#update_form").submit(function()
//    {
//
//
//        $.ajax({
//
//            url: '/admin/phonebook/ajax_edit',
//            type: 'POST',
//            dataType: 'json',
//            data:
//            {
//                id:        $("#id").val(),
//                name :     $(".name").val(),
//                phone:     $(".phone").val(),
//                email:     $(".email").val(),
//                address:   $(".address").val(),
//                state:     $(".state").val(),
//                zip:       $(".zip").val()
//            }
//
//        });
//
//    });

    $('.edit').on('click', function(){

        // Research JQuery $(this)
        // Research JQuery .parent()
        // Use $(this), .parent() and $(":input") selectors to select
        //   only the inputs in the same row as the edit button that was clicked.



        // Buttons
        $(this).hide();
        $(this).siblings('.submit').show();
        $(this).siblings('.delete').hide();

        var tr = $(this).closest('tr');
        tr.find(":input").attr('disabled', false);


//        $('.name').attr('disabled', false);
//        $('.phone').attr('disabled', false);
//        $('.email').attr('disabled', false);
//        $('.address').attr('disabled', false);
//        $('.state').attr('disabled', false);
//        $('.zip').attr('disabled', false);
    });

    $('.submit').on("click",  function(){


        //Buttons
        $(this).hide();
        $(this).siblings('.edit').show();
        $(this).siblings('.delete').show();


        var tr = $(this).closest('tr');
        tr.find(":text").attr('disabled', true);

        $.ajax({
            url: '/admin/phonebook/ajax_edit',
            type: 'POST',
            dataType: 'json',
            data:
            {
                id:        tr.attr('id'),
                name :     tr.find(":input[name=name]").val(),
                phone:     tr.find(":input[name=phone]").val(),
                email:     tr.find(":input[name=email]").val(),
                address:   tr.find(":input[name=address]").val(),
                state:     tr.find(":input[name=state]").val(),
                zip:       tr.find(":input[name=zip]").val()
            },
            success: function(output)
            {
                var string= '';
                string+='<div class="alert success animated fadeIn">' + output.responseText + '</div>';
                $('#error').html('').prepend(string);




            },
            error: function (output)
            {
                var string = '';
                string+='<div class="alert error animated fadeIn">' + output.message + '</div>';
                $('#error').html('').prepend(string);
                tr.find(":text['name']").val(output.contact.name);
                tr.find(":text['phone']").val(output.contact.phone);
                tr.find(":text['email']").val(output.contact.email);
                tr.find(":text['address']").val(output.contact.address);
                tr.find(":text['state']").val(output.contact.state);
                tr.find(":text['zip']").val(output.contact.zip);


            }


        });


    });

//        $('.name').attr('disabled', true);
//        $('.phone').attr('disabled', true);
//        $('.email').attr('disabled', true);
//        $('.address').attr('disabled', true);
//        $('.state').attr('disabled', true);
//        $('.zip').attr('disabled', true);
//    });

    $('.delete').click(function(){

        var tr = $(this).closest('tr');
        tr.find(":text").attr('disabled', true);

        $.ajax({
            url: '/admin/phonebook/ajax_delete',
            type: 'POST',
            dataType: 'json',
            data:
            {
                id:        tr.attr('id')
            }

    });
        $(this).closest('tr').remove();
    });

});




