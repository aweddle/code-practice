
<section class="item">
    <div class="content">
        <table border="0" class="table-list clear-both" id="tasks_table">
            <thead>
            <tr>
                <th><?php echo "Name"; ?></th>
                <th><?php echo "Phone"; ?></th>
                <th><?php echo "Email"; ?></th>
                <th><?php echo "Address"; ?></th>
                <th><?php echo "State"; ?></th>
                <th><?php echo "Zip"; ?></th>
                <th></th>
            </tr>
            </thead>
            <tbody id="filter_results">
            <?php foreach($info->result() as $info): ?>
                <tr id="<?php echo $info->id; ?>" class="bookRow">
                    <td><?php echo $info->name; ?></td>
                    <td><?php echo $info->phone;?></td>
                    <td><?php echo $info->email;?></td>
                    <td><?php echo $info->address;?></td>
                    <td><?php echo $info->state;?></td>
                    <td><?php echo $info->zip;?></td>
                    <td><?php echo anchor('admin/phonebook/editset/'. $info->id,'Edit','class="button edit"'); ?>
                    </td>

                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</section>
