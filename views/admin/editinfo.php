
<section class="item">
    <div id ='update_form' class="content">
        <table border="0" class="table-list clear-both" id="tasks_table">
            <thead>
            <tr>
                <th><?php echo "Name"; ?></th>
                <th><?php echo "Phone"; ?></th>
                <th><?php echo "Email"; ?></th>
                <th><?php echo "Address"; ?></th>
                <th><?php echo "State"; ?></th>
                <th><?php echo "Zip"; ?></th>
                <th></th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody id="update_form">
            <?php echo form_open($this->uri->uri_string(), 'id="update_form"'); ?>
                <tr id="<?php echo $info->id; ?>" class="bookRow">
                    <input type="hidden" id = 'id' name='id' value='<?php echo $info->id?>'>
                    <td><?php echo form_input(array('name'=>'name', 'value' => $info->name, "id"=>"name")); ?></td>
                    <td><?php echo form_input(array('name'=>'phone', 'value'=>$info->phone, 'id'=>'phone'));?></td>
                    <td><?php echo form_input(array('name'=>'email', 'value'=>$info->email, 'id'=>'email'));?></td>
                    <td><?php echo form_input(array('name'=>'address', 'value'=>$info->address, 'id'=>'address'));?></td>
                    <td><?php echo form_input(array('name'=>'state', 'value'=>$info->state, 'id'=>'state'));?></td>
                    <td><?php echo form_input(array('name'=>'zip', 'value'=>$info->zip, 'id'=>'zip'));?></td>
                    <td><input type=hidden id='submitted' name='submitted' value='submitted' /></td>
                    <td><?php echo form_submit('submit', 'Update') ?>
                    </td>

                </tr>
            </tbody>
            <?php echo form_close(); ?>
        </table>
    </div>
</section>
