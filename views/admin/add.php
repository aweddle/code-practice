<div id="error">
</div>
<div>
    <section class='title'>
        <h4><?php echo "Add a contact"; ?></h4>
    </section>
        <section>
        <table border="0">
            <thead>
            <tr>
                <th><?php echo "Name"; ?></th>
                <th><?php echo "Phone"; ?></th>
                <th><?php echo "Email"; ?></th>
                <th><?php echo "Address"; ?></th>
                <th><?php echo "State"; ?></th>
                <th><?php echo "Zip"; ?></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php echo form_open($this->uri->uri_string()); ?>
                <tr>
                    <td><?php echo form_input(array('name'=>'name', 'class'=>'name'));?></td>
                    <td><?php echo form_input(array('name'=>'phone', 'class'=>'phone'));?></td>
                    <td><?php echo form_input(array('name'=>'email', 'class'=>'email'));?></td>
                    <td><?php echo form_input(array('name'=>'address', 'class'=>'address'));?></td>
                    <td><?php echo form_input(array('name'=>'state', 'class'=>'state', 'maxlength' => '2', 'size' => '2'));?></td>
                    <td><?php echo form_input(array('name'=>'zip', 'class'=>'zip', 'maxlength' => '10', 'size' => '10'));?></td>
                    <td><?php echo form_button(array('name' => 'add', 'content'=>'Add', 'class'=>'add')); ?></td>
                </tr>
            </tbody>
            <?php echo form_close(); ?>
        </table>
        </section>
</div>


